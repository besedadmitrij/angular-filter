var phoneApp = angular.module('phoneApp', []);
phoneApp.controller('adressCtrl', function () {
	var self = this;
	self.contacts = [
		{
			name: "Lina"
			, surname: "Gaga"
			, phone: 111111
		}
		, {
			name: "A"
			, surname: "FF"
			, phone: 555555
		}
		, {
			name: "C"
			, surname: "BB"
			, phone: 444444
		}
		, {
			name: "B"
			, surname: "CC"
			, phone: 222222
		}
	];
	self.showInput = function () {
		self.inputField = true;
	}
	self.newContact = {};
	self.addContact = function () {
		self.contacts.push(self.newContact);
		self.inputField = false;
		console.log(self.newContact);
		self.newContact = {};
	};
	self.removeContact = function (contact) {
		var ind = self.contacts.indexOf(contact);
		self.contacts.splice(ind, 1);
	};
});
phoneApp.filter('filterByNameAndPhone', filterByNameAndPhone);

function filterByNameAndPhone() {
	return function (list, text) {
		if (!text) return list;
		text = text.toLowerCase();
		var result = [];
		for (var i = 0; i < list.length; i++) {
			var found = false;
			if (list[i]) {
				if (list[i].phone){ 
					var temp = list[i].phone.toString();
					if(temp.indexOf(text) !== -1) found = true;
				}
				if (list[i].name && list[i].name.toLowerCase().indexOf(text) !== -1) found = true;
				if (list[i].surname && list[i].surname.toLowerCase().indexOf(text) !== -1) found = true;
			}
			if (found) result.push(list[i]);
		}
		return result;
	}
};